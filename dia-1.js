//Exercicio 1

function fahrenheit(){
    const c = Math.round(((Math.random() * (500 - (-500)) + (-500)) - 32) * 5 / 9);
    console.log(c + " graus Celsius")
}

function aluno(){
    const notas = (Math.random() * 10 + Math.random() * 10 + Math.random() * 10) / 3;
    if(notas >= 6){
        console.log("Aprovado");
    }else{
        console.log("Reprovado");
    }
}

function fizzbuzz(){
    for(let i = 0; i < 5; i++){
        let a = Math.round(Math.random() * 100);
        console.log("Numero aleatorio: " + a);
        if (a % 3 === 0 && a % 5 === 0){
            console.log("fizzbuzz");
            continue;
        }else if (a % 3 === 0){
            console.log("fizz");
            continue;
        }else if (a % 5 === 0){
            console.log("buzz");
        }else{
            console.log("nada");
        }
    }
}